const branje = 6666
const branjeNaslova = 1700
const mrmranje = 1000

// Debug: za hitrenje
//const branje = 66
//const branjeNaslova = 10
//const mrmranje = 10

const naslov = [
    "MANIFEST",
    "SENZORIČNE",
    "POEZIJE"
]

const podnaslov = [
    "Finger",
    "nails",
    "on",
    "a",
    "Chalkboard",
    "(Ono",
    "mato",
    "poija",
    "na",
    "drugo",
    "žogo)"
]

const introTekst = [
    "Do sedaj so ob poeziji z vsemi čuti in čutili, telesno in fizično, trpeli samo pesniki – MORALI pa bi tudi poslušalci!",
    "Zavijanje z očmi, zehanje, ošinjanje urnih kazalcev – vsi ti preživetveni manevri, s katerimi se pretolčemo skozi bestialni napor literarnih branj, do sedaj še nikoli niso odtehtali torture pisanja literature;",
    "zlogov ritma, ki tolčejo ob oboke lobanje in se kot seizmografski zapisi utelešajo na papirju ali ekranu.",
    "Ti zahtevajo, da se k njim vedno znova vrača, kot Severin k udarcem svoje Wande – ker je pisanje disciplina, disciplina zaklinjanja razklenitve sintakse!",
    "Naloga in končni cilj senzorične poezije je zatorej samo ena: dlakocepsko cepiti živčne končiče – S CIRKULARKO!",
    "Senzorična poezija je poslednji glogov kol, zaboden v čifutsko srce vampirja, ki ne more umreti – Adorna!",
    "Poezija po holokavstu JE mogoča. Poezija 21. stoletja naj bo torej poezija Abu-Ghraiba in Guantanama, ali pa naj za vekomaj umolkne!",
    "Smrt vsem, živela Centrala!",
    "PSSS: Mučitelj hodi po literarnih večerih."
]

const za = x => ({
    vsebina: "",
    len: 0,
    pavza: x
})
const zb = {
    vsebina: "neprekinjeno",
    len: 650,
    pavza: 250
}
const zc = {
    vsebina: "nizanje",
    len: 400,
    pavza: 200
}
const zd = {
    vsebina: "istih",
    len: 350,
    pavza: 150
}
const ze = {
    vsebina: "glasov",
    len: 350,
    pavza: 100
}
const zf = {
    vsebina: "je",
    len: 160,
    pavza: 40
}
const zg = {
    vsebina: "dobesedno",
    len: 450,
    pavza: 200
}
const zgb = {
    vsebina: "dobesed",
    len: 400,
    pavza: 200
}
const zh = {
    vsebina: "slušni",
    len: 400,
    pavza: 100
}
const zi = {
    vsebina: "dražljaj",
    len: 666,
    pavza: 250
}
const strb = (len, pavza) => ({
    len,
    pavza
})

const zaporedje = [
    za(1200), zb, zc, zd, ze, zf, zg, zh, zi,
    za(300), zb, zc, zd, ze, zf, zgb, strb(1300, 0),
    za(0), zb, zc, zd, ze, zf, zg, zh, zi,
    za(300), zb, zc, zd, ze, zf, zgb, strb(1200, 0),
    za(0), zb, zc, zd, ze, zf, zg, zh, zi,
    za(0), zb, zc, zd, ze, zf, zgb, strb(1300, 0),
    za(0), zb, zc, zd, ze, zf, strb(1850, 0),
    za(0), zb, zc, zd, ze, zf, zg, strb(1200, 0),
]

const barvA = barva => barva ? "white" : "black"
const barvB = barva => barva ? "black" : "white"

function zamenjajBarve (barva) {
    document.body.style.backgroundColor = barvA(barva)
    document.crka.style.color = barvB(barva)
    document.barva = !barva
}

const zamenjajCrko = crka => document.crka.innerHTML = crka

function strobe (timestamp) {
    zamenjajBarve(document.barva)
    if (timestamp < document.koncajStrobe) requestAnimationFrame(strobe)
}

function startStrobe(timestamp) {
    document.koncajStrobe += timestamp
    requestAnimationFrame(strobe)
}

function animacija(kosi, stevec) {
    const konec = kosi.reduce((stk, kos) => {

        // Crka?
        if (kos.vsebina !== undefined) {
            kos.vsebina.split("").map((crka, idx, { length }) => {
                setTimeout(() => {
                    zamenjajBarve(document.barva)
                    zamenjajCrko(crka)
                }, stk + idx / length * kos.len)
            })

            setTimeout(() => zamenjajCrko(""), stk + kos.len + 0.05)
        //Strobe?
        } else {
            setTimeout(() => {
                zamenjajCrko("")
                document.koncajStrobe = kos.len
                requestAnimationFrame(startStrobe)
            }, stk)
        }

        return stk + kos.len + kos.pavza
    }, stevec)

    // Na koncu ponovimo vajo
    setTimeout(() => {
        zamenjajCrko("")

        animacija(zaporedje, 0)
        document.zvok.pause()
        document.zvok.currentTime = 0
        document.zvok.play()
    }, konec)
}

const podnaslovljenje = (kosi, stevec) => {
    kosi.map((kos, idx) => setTimeout(() => {
        zamenjajCrko(kos)
    }, stevec + idx * branjeNaslova))

    const pricetek = stevec + kosi.length * branjeNaslova + mrmranje
    setTimeout(() => {
        document.crka.style.fontSize = "16rem"
        document.zvok.play()
    }, pricetek)
    animacija(zaporedje, pricetek)
}

const uvod = (kosi, stevec) => {
    kosi.map((kos, idx) => setTimeout(() => {
        zamenjajCrko(kos)
    }, stevec + idx * branje))

    const pricetek = stevec + kosi.length * branje
    setTimeout(() => {
        document.crka.style.fontSize = "12rem"
    }, pricetek)
    podnaslovljenje(podnaslov, pricetek)
}

const naslovljenje = (kosi, stevec) => {
    kosi.map((kos, idx) => setTimeout(() => {
        zamenjajCrko(kos)
    }, stevec + idx * branjeNaslova))

    const pricetek = stevec + kosi.length * branjeNaslova + mrmranje
    setTimeout(() => {
        document.crka.style.fontSize = "4rem"
    }, pricetek)
    uvod(introTekst, pricetek)
}

const zacetek = () => {
    document.crka.style.fontSize = "12rem"
    naslovljenje(naslov, mrmranje)
}

document.addEventListener('DOMContentLoaded', () => {
    document.barva = true
    document.crka = document.getElementById("crka")
    document.zvok = document.getElementById("zvok")
    document.zamenjajBarve = zamenjajBarve

    document.body.style.backgroundColor = "white"
    document.crka.style.color = "black"
})

window.addEventListener("mousedown", () => {
    document.body.style.cursor = "none"
    document.getElementById("navodila").style.display = "none"

    if (!Modernizr.audio.ogg) {
        document.getElementById("fejl").style.display = "block"
    }

    if (!document.zacetek) {
        document.zacetek = true
        zacetek()
    }
})
